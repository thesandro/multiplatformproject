package com.example.mykmmapp.androidApp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mykmmapp.shared.Core
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {
    lateinit var adapter: RedditPostRecyclerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var nextPage = ""
        var isLoading = false

        val recyclerView = findViewById<RecyclerView>(R.id.redditPostRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RedditPostRecyclerAdapter()
        //adapter.setHasStableIds(true)
        recyclerView.adapter = adapter
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == adapter.postList.size - 1) {
                        //bottom of list!
                        CoroutineScope(Dispatchers.Main).launch {
                            val response = Core().getRedditData(nextPage)
                            nextPage = response.data.after
                            adapter.postList.addAll(response.data.children)
                            adapter.notifyDataSetChanged()
                            isLoading = false
                        }
                        isLoading = true
                    }
                }
            }
        })

 //       val tv: TextView = findViewById(R.id.text_view)
        CoroutineScope(Dispatchers.Main).launch {
            val response = Core().getRedditData("")
            nextPage = response.data.after
            adapter.postList.addAll(response.data.children)
            adapter.notifyDataSetChanged()
        }

    }
}
