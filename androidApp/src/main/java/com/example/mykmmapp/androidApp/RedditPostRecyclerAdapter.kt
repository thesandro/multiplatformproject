package com.example.mykmmapp.androidApp

import android.content.Context
import android.util.DisplayMetrics
import android.view.Display
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.mykmmapp.shared.RedditData


class RedditPostRecyclerAdapter: RecyclerView.Adapter<RedditPostRecyclerAdapter.ViewHolder>() {
    var postList = ArrayList<RedditData.Data.Children>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.reddit_post_recycler_view_item,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun getItemCount() = postList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply()
    }

    override fun onViewRecycled(holder: ViewHolder) {
        val postImage = holder.itemView.findViewById<ImageView>(R.id.postImageView)
        Glide.with(postImage).clear(postImage)
        super.onViewRecycled(holder)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun apply(){
            val model = postList[adapterPosition]
            val postImage = itemView.findViewById<ImageView>(R.id.postImageView)
            val postTitle = itemView.findViewById<TextView>(R.id.postTitle)
            postTitle.text = model.data.title
            val sourceWidth = model.data.preview.images[0].source.width
            val sourceHeight = model.data.preview.images[0].source.height
            val scaledWidth = if(sourceWidth > itemView.resources.displayMetrics.widthPixels) itemView.resources.displayMetrics.widthPixels else sourceWidth
            val scaledHeight = sourceHeight*scaledWidth/sourceWidth
            postImage.layoutParams.width = scaledWidth
            postImage.layoutParams.height = scaledHeight
            Glide.with(itemView).load(model.data.url).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).into(postImage)
        }
    }
}