//
//  ViewController.swift
//  TestApp
//
//  Created by Sandro on 11/10/20.
//

import UIKit
import shared
import AlamofireImage

func greet() -> String {
    return Greeting().greeting()
}


class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var redditData:RedditData? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "RedditPostCell", bundle: nil), forCellReuseIdentifier: "redditCell")
        // Do any additional setup after loading the view.
        Greeting().getRedditData(completionHandler: { data,_ in
            //self.image.af.setImage(withURL: URL(string: data!.data.children[0].data.thumbnail)!)
            //self.text.text = data!.data.children[0].data.subreddit
            self.redditData = data
            self.tableView.reloadData()
            
        })
    }
}

extension ViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return redditData?.data.children.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "redditCell", for: indexPath) as! RedditPostCell
        if(URL(string: (redditData?.data.children[indexPath.row].data.thumbnail)!) != nil){
            cell.postImage.af.setImage(withURL: URL(string: (redditData?.data.children[indexPath.row].data.url)!)!)
        }
            
        cell.postLabel!.text = redditData?.data.children[indexPath.row].data.title
        return cell
    }
}
