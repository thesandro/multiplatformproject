//
//  RedditPostCell.swift
//  iosApp
//
//  Created by Sandro Kakhetelidze on 11/19/20.
//  Copyright © 2020 orgName. All rights reserved.
//

import UIKit

class RedditPostCell: UITableViewCell {

    @IBOutlet var postImage: UIImageView!
    @IBOutlet var postLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
