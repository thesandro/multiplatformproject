package com.example.mykmmapp.shared


class Core{

    val redditDataSource = arrayListOf<RedditData>()

    suspend fun getRedditData(page:String):RedditData{
        return ApiService.getSubRedditData("pic",page)
    }
}
