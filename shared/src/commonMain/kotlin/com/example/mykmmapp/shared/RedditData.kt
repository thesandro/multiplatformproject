package com.example.mykmmapp.shared


import kotlinx.serialization.Contextual
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RedditData(
    @SerialName("data")
    val `data`: Data = Data(),
    @SerialName("kind")
    val kind: String = ""
) {
    @Serializable
    data class Data(
        @SerialName("after")
        val after: String = "",
        @SerialName("children")
        val children: List<Children> = listOf(),
        @SerialName("dist")
        val dist: Int = 0,
        @SerialName("modhash")
        val modhash: String = ""
    ) {
        @Serializable
        data class Children(
            @SerialName("data")
            val `data`: Data = Data(),
            @SerialName("kind")
            val kind: String = ""
        ) {
            @Serializable
            data class Data(
                @SerialName("all_awardings")
                val allAwardings: List<AllAwarding> = listOf(),
                @SerialName("allow_live_comments")
                val allowLiveComments: Boolean = false,
                @SerialName("archived")
                val archived: Boolean = false,
                @SerialName("author")
                val author: String = "",
                @SerialName("author_flair_richtext")
                val authorFlairRichtext: List<AuthorFlairRichtext> = listOf(),
                @SerialName("author_flair_type")
                val authorFlairType: String = "",
                @SerialName("author_fullname")
                val authorFullname: String = "",
                @SerialName("author_patreon_flair")
                val authorPatreonFlair: Boolean = false,
                @SerialName("author_premium")
                val authorPremium: Boolean = false,
                @SerialName("can_gild")
                val canGild: Boolean = false,
                @SerialName("can_mod_post")
                val canModPost: Boolean = false,
                @SerialName("clicked")
                val clicked: Boolean = false,
                @SerialName("content_categories")
                val contentCategories: List<String> = listOf(),
                @SerialName("contest_mode")
                val contestMode: Boolean = false,
                @SerialName("created")
                val created: Double = 0.0,
                @SerialName("created_utc")
                val createdUtc: Double = 0.0,
                @SerialName("domain")
                val domain: String = "",
                @SerialName("downs")
                val downs: Int = 0,
                @SerialName("gilded")
                val gilded: Int = 0,
                @SerialName("gildings")
                val gildings: Gildings = Gildings(),
                @SerialName("hidden")
                val hidden: Boolean = false,
                @SerialName("hide_score")
                val hideScore: Boolean = false,
                @SerialName("id")
                val id: String = "",
                @SerialName("is_crosspostable")
                val isCrosspostable: Boolean = false,
                @SerialName("is_meta")
                val isMeta: Boolean = false,
                @SerialName("is_original_content")
                val isOriginalContent: Boolean = false,
                @SerialName("is_reddit_media_domain")
                val isRedditMediaDomain: Boolean = false,
                @SerialName("is_robot_indexable")
                val isRobotIndexable: Boolean = false,
                @SerialName("is_self")
                val isSelf: Boolean = false,
                @SerialName("is_video")
                val isVideo: Boolean = false,
                @SerialName("link_flair_background_color")
                val linkFlairBackgroundColor: String = "",
                @SerialName("link_flair_css_class")
                val linkFlairCssClass: String = "",
                @SerialName("link_flair_richtext")
                val linkFlairRichtext: List<LinkFlairRichtext> = listOf(),
                @SerialName("link_flair_template_id")
                val linkFlairTemplateId: String = "",
                @SerialName("link_flair_text")
                val linkFlairText: String = "",
                @SerialName("link_flair_text_color")
                val linkFlairTextColor: String = "",
                @SerialName("link_flair_type")
                val linkFlairType: String = "",
                @SerialName("locked")
                val locked: Boolean = false,
                @SerialName("media_embed")
                val mediaEmbed: MediaEmbed = MediaEmbed(),
                @SerialName("media_only")
                val mediaOnly: Boolean = false,
                @SerialName("name")
                val name: String = "",
                @SerialName("no_follow")
                val noFollow: Boolean = false,
                @SerialName("num_comments")
                val numComments: Int = 0,
                @SerialName("num_crossposts")
                val numCrossposts: Int = 0,
                @SerialName("parent_whitelist_status")
                val parentWhitelistStatus: String = "",
                @SerialName("permalink")
                val permalink: String = "",
                @SerialName("pinned")
                val pinned: Boolean = false,
                @SerialName("post_hint")
                val postHint: String = "",
                @SerialName("preview")
                val preview: Preview = Preview(),
                @SerialName("pwls")
                val pwls: Int = 0,
                @SerialName("quarantine")
                val quarantine: Boolean = false,
                @SerialName("saved")
                val saved: Boolean = false,
                @SerialName("score")
                val score: Int = 0,
                @SerialName("secure_media_embed")
                val secureMediaEmbed: SecureMediaEmbed = SecureMediaEmbed(),
                @SerialName("selftext")
                val selftext: String = "",
                @SerialName("send_replies")
                val sendReplies: Boolean = false,
                @SerialName("spoiler")
                val spoiler: Boolean = false,
                @SerialName("stickied")
                val stickied: Boolean = false,
                @SerialName("subreddit")
                val subreddit: String = "",
                @SerialName("subreddit_id")
                val subredditId: String = "",
                @SerialName("subreddit_name_prefixed")
                val subredditNamePrefixed: String = "",
                @SerialName("subreddit_subscribers")
                val subredditSubscribers: Int = 0,
                @SerialName("subreddit_type")
                val subredditType: String = "",
                @SerialName("thumbnail")
                val thumbnail: String = "",
                @SerialName("thumbnail_height")
                val thumbnailHeight: Int = 0,
                @SerialName("thumbnail_width")
                val thumbnailWidth: Int = 0,
                @SerialName("title")
                val title: String = "",
                @SerialName("total_awards_received")
                val totalAwardsReceived: Int = 0,
                @SerialName("ups")
                val ups: Int = 0,
                @SerialName("upvote_ratio")
                val upvoteRatio: Double = 0.0,
                @SerialName("url")
                val url: String = "",
                @SerialName("url_overridden_by_dest")
                val urlOverriddenByDest: String = "",
                @SerialName("visited")
                val visited: Boolean = false,
                @SerialName("whitelist_status")
                val whitelistStatus: String = "",
                @SerialName("wls")
                val wls: Int = 0
            ) {
                @Serializable
                data class AllAwarding(
                    @SerialName("award_sub_type")
                    val awardSubType: String = "",
                    @SerialName("award_type")
                    val awardType: String = "",
                    @SerialName("coin_price")
                    val coinPrice: Int = 0,
                    @SerialName("coin_reward")
                    val coinReward: Int = 0,
                    @SerialName("count")
                    val count: Int = 0,
                    @SerialName("days_of_drip_extension")
                    val daysOfDripExtension: Int = 0,
                    @SerialName("days_of_premium")
                    val daysOfPremium: Int = 0,
                    @SerialName("description")
                    val description: String = "",
                    @SerialName("icon_height")
                    val iconHeight: Int = 0,
                    @SerialName("icon_url")
                    val iconUrl: String = "",
                    @SerialName("icon_width")
                    val iconWidth: Int = 0,
                    @SerialName("id")
                    val id: String = "",
                    @SerialName("is_enabled")
                    val isEnabled: Boolean = false,
                    @SerialName("is_new")
                    val isNew: Boolean = false,
                    @SerialName("name")
                    val name: String = "",
                    @SerialName("resized_icons")
                    val resizedIcons: List<ResizedIcon> = listOf(),
                    @SerialName("resized_static_icons")
                    val resizedStaticIcons: List<ResizedStaticIcon> = listOf(),
                    @SerialName("static_icon_height")
                    val staticIconHeight: Int = 0,
                    @SerialName("static_icon_url")
                    val staticIconUrl: String = "",
                    @SerialName("static_icon_width")
                    val staticIconWidth: Int = 0,
                    @SerialName("subreddit_coin_reward")
                    val subredditCoinReward: Int = 0
                ) {
                    @Serializable
                    data class ResizedIcon(
                        @SerialName("height")
                        val height: Int = 0,
                        @SerialName("url")
                        val url: String = "",
                        @SerialName("width")
                        val width: Int = 0
                    )

                    @Serializable
                    data class ResizedStaticIcon(
                        @SerialName("height")
                        val height: Int = 0,
                        @SerialName("url")
                        val url: String = "",
                        @SerialName("width")
                        val width: Int = 0
                    )
                }

                @Serializable
                data class AuthorFlairRichtext(
                    @SerialName("a")
                    val a: String = "",
                    @SerialName("e")
                    val e: String = "",
                    @SerialName("t")
                    val t: String = "",
                    @SerialName("u")
                    val u: String = ""
                )

                @Serializable
                data class Gildings(
                    @SerialName("gid_1")
                    val gid1: Int = 0,
                    @SerialName("gid_2")
                    val gid2: Int = 0
                )

                @Serializable
                data class LinkFlairRichtext(
                    @SerialName("e")
                    val e: String = "",
                    @SerialName("t")
                    val t: String = ""
                )

                @Serializable
                data class MediaEmbed(
                    @SerialName("content")
                    val content: String = "",
                    @SerialName("height")
                    val height: Int = 0,
                    @SerialName("scrolling")
                    val scrolling: Boolean = false,
                    @SerialName("width")
                    val width: Int = 0
                )

                @Serializable
                data class Preview(
                    @SerialName("enabled")
                    val enabled: Boolean = false,
                    @SerialName("images")
                    val images: List<Image> = listOf(),
                    @SerialName("reddit_video_preview")
                    val redditVideoPreview: RedditVideoPreview = RedditVideoPreview()
                ) {
                    @Serializable
                    data class Image(
                        @SerialName("id")
                        val id: String = "",
                        @SerialName("resolutions")
                        val resolutions: List<Resolution> = listOf(),
                        @SerialName("source")
                        val source: Source = Source(),
                        @SerialName("variants")
                        val variants: Variants = Variants()
                    ) {
                        @Serializable
                        data class Resolution(
                            @SerialName("height")
                            val height: Int = 0,
                            @SerialName("url")
                            val url: String = "",
                            @SerialName("width")
                            val width: Int = 0
                        )

                        @Serializable
                        data class Source(
                            @SerialName("height")
                            val height: Int = 0,
                            @SerialName("url")
                            val url: String = "",
                            @SerialName("width")
                            val width: Int = 0
                        )

                        @Serializable
                        data class Variants(
                            @SerialName("gif")
                            val gif: Gif = Gif(),
                            @SerialName("mp4")
                            val mp4: Mp4 = Mp4(),
                            @SerialName("nsfw")
                            val nsfw: Nsfw = Nsfw(),
                            @SerialName("obfuscated")
                            val obfuscated: Obfuscated = Obfuscated()
                        ) {
                            @Serializable
                            data class Gif(
                                @SerialName("resolutions")
                                val resolutions: List<Resolution> = listOf(),
                                @SerialName("source")
                                val source: Source = Source()
                            ) {
                                @Serializable
                                data class Resolution(
                                    @SerialName("height")
                                    val height: Int = 0,
                                    @SerialName("url")
                                    val url: String = "",
                                    @SerialName("width")
                                    val width: Int = 0
                                )

                                @Serializable
                                data class Source(
                                    @SerialName("height")
                                    val height: Int = 0,
                                    @SerialName("url")
                                    val url: String = "",
                                    @SerialName("width")
                                    val width: Int = 0
                                )
                            }

                            @Serializable
                            data class Mp4(
                                @SerialName("resolutions")
                                val resolutions: List<Resolution> = listOf(),
                                @SerialName("source")
                                val source: Source = Source()
                            ) {
                                @Serializable
                                data class Resolution(
                                    @SerialName("height")
                                    val height: Int = 0,
                                    @SerialName("url")
                                    val url: String = "",
                                    @SerialName("width")
                                    val width: Int = 0
                                )

                                @Serializable
                                data class Source(
                                    @SerialName("height")
                                    val height: Int = 0,
                                    @SerialName("url")
                                    val url: String = "",
                                    @SerialName("width")
                                    val width: Int = 0
                                )
                            }

                            @Serializable
                            data class Nsfw(
                                @SerialName("resolutions")
                                val resolutions: List<Resolution> = listOf(),
                                @SerialName("source")
                                val source: Source = Source()
                            ) {
                                @Serializable
                                data class Resolution(
                                    @SerialName("height")
                                    val height: Int = 0,
                                    @SerialName("url")
                                    val url: String = "",
                                    @SerialName("width")
                                    val width: Int = 0
                                )

                                @Serializable
                                data class Source(
                                    @SerialName("height")
                                    val height: Int = 0,
                                    @SerialName("url")
                                    val url: String = "",
                                    @SerialName("width")
                                    val width: Int = 0
                                )
                            }

                            @Serializable
                            data class Obfuscated(
                                @SerialName("resolutions")
                                val resolutions: List<Resolution> = listOf(),
                                @SerialName("source")
                                val source: Source = Source()
                            ) {
                                @Serializable
                                data class Resolution(
                                    @SerialName("height")
                                    val height: Int = 0,
                                    @SerialName("url")
                                    val url: String = "",
                                    @SerialName("width")
                                    val width: Int = 0
                                )

                                @Serializable
                                data class Source(
                                    @SerialName("height")
                                    val height: Int = 0,
                                    @SerialName("url")
                                    val url: String = "",
                                    @SerialName("width")
                                    val width: Int = 0
                                )
                            }
                        }
                    }

                    @Serializable
                    data class RedditVideoPreview(
                        @SerialName("dash_url")
                        val dashUrl: String = "",
                        @SerialName("duration")
                        val duration: Int = 0,
                        @SerialName("fallback_url")
                        val fallbackUrl: String = "",
                        @SerialName("height")
                        val height: Int = 0,
                        @SerialName("hls_url")
                        val hlsUrl: String = "",
                        @SerialName("is_gif")
                        val isGif: Boolean = false,
                        @SerialName("scrubber_media_url")
                        val scrubberMediaUrl: String = "",
                        @SerialName("transcoding_status")
                        val transcodingStatus: String = "",
                        @SerialName("width")
                        val width: Int = 0
                    )
                }

                @Serializable
                data class SecureMediaEmbed(
                    @SerialName("content")
                    val content: String = "",
                    @SerialName("height")
                    val height: Int = 0,
                    @SerialName("media_domain_url")
                    val mediaDomainUrl: String = "",
                    @SerialName("scrolling")
                    val scrolling: Boolean = false,
                    @SerialName("width")
                    val width: Int = 0
                )
            }
        }
    }
}