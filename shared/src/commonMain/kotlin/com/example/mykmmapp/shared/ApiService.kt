package com.example.mykmmapp.shared

import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

object ApiService {
    private const val BASE_PATH = "https://www.reddit.com"
    private val ktorClient =  HttpClient()

    suspend fun getSubRedditData(subreddit:String,page:String):RedditData{
        val response = ktorClient.get<HttpResponse>( "$BASE_PATH/r/$subreddit.json?after=$page") {
            headers {
                append("Accept", "application/json")
            }
        }
        val stringResponse = response.readText()
        return Json {
            ignoreUnknownKeys = true
            coerceInputValues = true
        }.decodeFromString(stringResponse)
    }
}